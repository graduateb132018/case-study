echo off
echo Starting tomcat container, can take approximately 1 minute to complete, please wait...
docker run -p 8090:8080 -dit --name t9s-dbda-90 -v C:\vm_share\tomcat-webapps\dbanalyzer:/usr/local/tomcat/webapps/dbanalyzer.war tomcat9-server
timeout 30 /NOBREAK

echo ..
echo ..
echo tomcat should now be available
echo
