DROP view IF EXISTS `tab1`;
DROP view IF EXISTS `tab2`;
DROP view IF EXISTS `reqone`;
create view tab1 as
(select instrument_name as instr_id1, avg(deal_amount) as BUY
from deal,instrument
where deal_type = 'B' and deal_instrument_id = instrument_id
group by deal_instrument_id);

create view tab2 as
(select instrument_name as instr_id2, avg(deal_amount) as SELL
from deal,instrument
where deal_type = 'S' and deal_instrument_id = instrument_id
group by deal_instrument_id);

create view reqone as
(select instr_id1, BUY, SELL from tab1
join tab2 on instr_id1=instr_id2);

DROP view IF EXISTS `tab2_1`;
DROP view IF EXISTS `tab2_2`;
DROP view IF EXISTS `reqtwo`;
DROP view IF EXISTS `req2for3`;



create view tab2_1 as
(select instrument_name as instr_id1, sum(deal_quantity) as BUY, counterparty_name as deal_id1
from deal, instrument, counterparty
where deal_type = 'B' and instrument_id=deal_instrument_id and deal_counterparty_id = counterparty_id
group by deal_counterparty_id, instrument_name);

create view tab2_2 as
(select instrument_name as instr_id2,  sum(deal_quantity) as SELL, counterparty_name as deal_id2
from deal, instrument, counterparty
where deal_type = 'S' and instrument_id=deal_instrument_id and deal_counterparty_id = counterparty_id
group by deal_counterparty_id, instrument_name);

create view reqtwo as
(select instr_id1 as instr_id,deal_id1 as id, BUY, SELL, (SELL - BUY) as dif 
from tab2_1
join tab2_2 on instr_id1=instr_id2
group by id, instr_id
order by instr_id, dif desc
);

create view req2for3 as
(select deal_id1 as id, sum(BUY) as BUYs, sum(SELL) as SELLs, (sum(SELL) - sum(BUY)) as dif  
from tab2_1
join tab2_2 on instr_id1=instr_id2
group by deal_id1
order by dif desc
);

DROP view IF EXISTS `tab3_1`;
DROP view IF EXISTS `tab3_2`;
DROP view IF EXISTS `req3`;

create view tab3_1 as
(select avg(deal_amount) as BUY_pr, sum(deal_quantity) as SELL_s, counterparty_name as deal_id1
from deal, counterparty
where deal_type = 'B' and deal_counterparty_id = counterparty_id 
group by counterparty_id);

create view tab3_2 as
(select avg(deal_amount) as SELL_pr, sum(deal_quantity) as SELL_q, counterparty_name as deal_id2
from deal, counterparty
where deal_type = 'S' and deal_counterparty_id = counterparty_id 
group by counterparty_id);

create view req3 as
(select deal_id1 as id, (SELL_pr - BUY_pr) * SELL_q as result    
from tab3_1
join tab3_2 on deal_id2=deal_id1
order by result desc)
;

DROP view IF EXISTS `tab4_1`;
DROP view IF EXISTS `tab4_1b`;
DROP view IF EXISTS `tab4_1s`;
DROP view IF EXISTS `req4`;
DROP view IF EXISTS `RESULT4`;







create view tab4_1 as
select max(deal_time), deal_type, deal_amount, deal_quantity, counterparty_name as deal_counterparty_id
from (select * from deal,counterparty where deal_type = 'B' and deal_counterparty_id = counterparty_id order by deal_time asc) x
group by deal_counterparty_id
union
select max(deal_time), deal_type, deal_amount, deal_quantity, counterparty_name as deal_counterparty_id
from (select * from deal,counterparty where deal_type = 'S' and deal_counterparty_id = counterparty_id order by deal_time asc ) x
group by deal_counterparty_id;

create view tab4_1s as
select deal_counterparty_id, deal_amount as aq
from tab4_1
where deal_type = 'S'
group by deal_counterparty_id
;
create view tab4_1b as
select deal_counterparty_id, deal_amount as aq
from tab4_1
where deal_type = 'B'
group by deal_counterparty_id
;



create view req4 as
(select tab4_1s.deal_counterparty_id as id, tab4_1s.aq as Sell,tab4_1b.aq as buy
from tab4_1s, tab4_1b
where tab4_1b.deal_counterparty_id = tab4_1s.deal_counterparty_id)
;


create view RESULT4 as
(select req4.id as id, (req3.result + (req2for3.dif)*((case when (req3.result>0) then req4.Sell else req4.buy end) - tab3_2.SELL_pr)) 
from req3, req2for3, req4, tab3_2, deal, counterparty
where req4.id =req3.id and req4.id = req2for3.id  and req4.id = tab3_2.deal_id2  
group by req4.id
);
