SET autocommit = 0;

START TRANSACTION;

RENAME TABLE deal TO deal_old;

CREATE TABLE `deal` (
  `deal_id` int(11) NOT NULL,
  `deal_time` varchar(30) NOT NULL,
  `deal_type` char(1) DEFAULT NULL,
  `deal_amount` decimal(12,2) DEFAULT NULL,
  `deal_quantity` int(11) NOT NULL,
  `deal_instrument_id` int(11) NOT NULL,
  `deal_counterparty_id` int(11) NOT NULL,
  PRIMARY KEY (`deal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `instrument` (
  `instrument_id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `instrument_name` varchar(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `counterparty` (
  `counterparty_id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `counterparty_name` char(30) NOT NULL,
  `counterparty_status` char(1) DEFAULT NULL,
  `counterparty_date_registered` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO instrument (instrument_name)
(
    SELECT instrument_name
    FROM db_grad.deal_old
    group by instrument_name
);

INSERT INTO counterparty (counterparty_name, counterparty_status, counterparty_date_registered)
(
    SELECT counterparty_name, counterparty_status, counterparty_date_registered
    FROM db_grad.deal_old
    group by counterparty_name, counterparty_status, counterparty_date_registered
);

ALTER TABLE deal
ADD FOREIGN KEY (deal_instrument_id) REFERENCES instrument(instrument_id);

ALTER TABLE deal
ADD FOREIGN KEY (deal_counterparty_id) REFERENCES counterparty(counterparty_id);

INSERT INTO deal (deal_id, deal_time,deal_type, deal_amount, deal_quantity, deal_instrument_id, deal_counterparty_id)
(
    SELECT deal_id, deal_time,deal_type, deal_amount, deal_quantity, i.instrument_id, c.counterparty_id
    FROM deal_old o
    INNER JOIN instrument i ON o.instrument_name = i.instrument_name
    INNER JOIN counterparty c ON o.counterparty_name = c.counterparty_name
        AND o.counterparty_status = c.counterparty_status
        AND o.counterparty_date_registered = c.counterparty_date_registered
);

COMMIT;









