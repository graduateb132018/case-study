package com.db.casestudy.core.impl;

import com.db.casestudy.core.api.CoreException;
import com.db.casestudy.core.api.entity.User;
import com.db.casestudy.core.api.repository.UserRepository;
import com.db.casestudy.core.connection.Connector;
import com.db.casestudy.core.util.Log;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UserRepositoryImpl implements UserRepository {
    private static final Logger LOG = Log.getLogger();

    @Override
    public User getUserByUsername(String username) throws CoreException {
        try (Connection connection = Connector.getInstance().getConnection()) {
            try (PreparedStatement preparedStatement = connection
                    .prepareStatement("SELECT user_id, user_pwd FROM users WHERE user_id = ? ;")) {
                preparedStatement.setString(1, username);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (resultSet.next()) {
                        return new User(username, resultSet.getString("user_pwd"));
                    } else {
                        return null;
                    }
                }
            }
        } catch (SQLException e) {
            LOG.log(Level.SEVERE, "Exception thrown: ", e);
            throw new CoreException(e);
        }
    }
}
