package com.db.casestudy.core.impl;

import com.db.casestudy.core.api.CoreException;
import com.db.casestudy.core.api.entity.Counterparty;
import com.db.casestudy.core.api.entity.CounterpartyNameDealTypeCount;
import com.db.casestudy.core.api.entity.Deal;
import com.db.casestudy.core.api.entity.Instrument;
import com.db.casestudy.core.api.repository.DealRepository;
import com.db.casestudy.core.api.repository.request.FilteredSortedPageRequest;
import com.db.casestudy.core.connection.Connector;
import com.db.casestudy.core.util.Log;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DealRepositoryImpl implements DealRepository {
    private static final Logger LOG = Log.getLogger();

    @Override
    public List<Deal> getAll() throws CoreException {
        try (Connection connection = Connector.getInstance().getConnection()) {
            try (Statement statement = connection.createStatement()) {
                try (ResultSet resultSet = statement.executeQuery(DealRepositoryQueryConstructor.SELECT_ALL_QUERY)) {
                    return extractDealsFromResultSet(resultSet);
                }
            }
        } catch (SQLException e) {
            LOG.log(Level.SEVERE, "Exception thrown: ", e);
            throw new CoreException(e);
        }
    }

    @Override
    public List<Deal> getPage(FilteredSortedPageRequest<Deal> dealFilteredSortedPageRequest) throws CoreException {
        try (Connection connection = Connector.getInstance().getConnection()) {
            DealRepositoryQueryConstructor queryConstructor = new DealRepositoryQueryConstructor(dealFilteredSortedPageRequest);
            try (PreparedStatement preparedStatement = connection
                    .prepareStatement(queryConstructor.constructPageSqlForPreparedStatement())) {
                fillPreparedStatement(preparedStatement, dealFilteredSortedPageRequest);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    return extractDealsFromResultSet(resultSet);
                }
            }
        } catch (SQLException e) {
            LOG.log(Level.SEVERE, "Exception thrown: ", e);
            throw new CoreException(e);
        }
    }

    private static List<Deal> extractDealsFromResultSet(ResultSet resultSet) throws SQLException {
        List<Deal> deals = new ArrayList<>();
        while (resultSet.next()) {
            int id = resultSet.getInt("d.deal_id");
            String time = resultSet.getString("d.deal_time");
            char type = resultSet.getString("d.deal_type").charAt(0);
            double amount = resultSet.getDouble("d.deal_amount");
            int quantity = resultSet.getInt("d.deal_quantity");
            int instrumentId = resultSet.getInt("d.deal_instrument_id");
            String instrumentName = resultSet.getString("i.instrument_name");
            int counterpartyId = resultSet.getInt("d.deal_counterparty_id");
            String counterpartyName = resultSet.getString("c.counterparty_name");
            char counterpartyStatus = resultSet.getString("c.counterparty_status").charAt(0);
            LocalDateTime counterparty_date_registered = resultSet.getTimestamp("c.counterparty_date_registered").toLocalDateTime();

            deals.add(new Deal(id, time, type, amount, quantity,
                    new Instrument(instrumentId, instrumentName),
                    new Counterparty(counterpartyId, counterpartyName, counterpartyStatus, counterparty_date_registered)));
        }
        return deals;
    }

    @Override
    public int getTotalRows(FilteredSortedPageRequest<Deal> dealFilteredSortedPageRequest) throws CoreException {
        try (Connection connection = Connector.getInstance().getConnection()) {
            DealRepositoryQueryConstructor queryConstructor = new DealRepositoryQueryConstructor(dealFilteredSortedPageRequest);
            try (PreparedStatement preparedStatement = connection
                    .prepareStatement(queryConstructor.constructCountSqlForPreparedStatement())) {
                fillPreparedStatement(preparedStatement, dealFilteredSortedPageRequest);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (resultSet.next()) {
                        return resultSet.getInt("C");
                    } else {
                        return -1;
                    }
                }
            }
        } catch (SQLException e) {
            LOG.log(Level.SEVERE, "Exception thrown: ", e);
            throw new CoreException(e);
        }
    }


    private static void fillPreparedStatement(PreparedStatement ps, FilteredSortedPageRequest<Deal> r) throws SQLException {
        int j = 1;
        for (int i = 0; i < r.getFilters().size(); i++) {
            ps.setString(j++, r.getFilters().get(i).getKeyWord());
        }
    }

    private final static String COUNTERPARTY_NAME_DEAL_TYPE_COUNT_SQL =
            "SELECT SUM(deal_quantity) as COUNT, counterparty_name, deal_type\n" +
                    "FROM deal d\n" +
                    "       JOIN counterparty c ON d.deal_counterparty_id = c.counterparty_id\n" +
                    "GROUP BY deal_counterparty_id, deal_type;";

    @Override
    public List<CounterpartyNameDealTypeCount> getCounterpartyNameDealTypeCount() throws CoreException {
        try (Connection connection = Connector.getInstance().getConnection()) {
            try (Statement statement = connection.createStatement()) {
                try (ResultSet resultSet = statement.executeQuery(COUNTERPARTY_NAME_DEAL_TYPE_COUNT_SQL)) {
                    List<CounterpartyNameDealTypeCount> list = new ArrayList<>();
                    while (resultSet.next()) {
                        list.add(new CounterpartyNameDealTypeCount(
                                resultSet.getLong("COUNT"),
                                resultSet.getString("counterparty_name"),
                                resultSet.getString("deal_type").charAt(0)));
                    }
                    return list;
                }
            }
        } catch (SQLException e) {
            LOG.log(Level.SEVERE, "Exception thrown: ", e);
            throw new CoreException(e);
        }
    }
}
