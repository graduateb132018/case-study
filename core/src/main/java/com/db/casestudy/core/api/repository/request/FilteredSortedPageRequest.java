package com.db.casestudy.core.api.repository.request;

import com.db.casestudy.core.api.entity.Entity;
import com.db.casestudy.core.api.repository.filter.Filter;
import com.db.casestudy.core.api.repository.sort.Sort;

import java.util.List;

public interface FilteredSortedPageRequest<E extends Entity> {
    int getPageNumber();

    int getEntryNumber();

    List<Sort<E>> getSorts();

    List<Filter<E>> getFilters();
}
