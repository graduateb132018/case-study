package com.db.casestudy.core.api.repository.filter;

import com.db.casestudy.core.api.column.Column;
import com.db.casestudy.core.api.entity.Entity;

public interface Filter<E extends Entity> {
    Column<E> getColumn();

    String getKeyWord();
}
