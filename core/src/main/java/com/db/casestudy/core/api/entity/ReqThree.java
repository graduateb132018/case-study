package com.db.casestudy.core.api.entity;

public class ReqThree {
    private final String id;
    private final long result;

    public ReqThree(String id, long result) {
        this.id = id;
        this.result = result;
    }

    public String getId() {
        return id;
    }

    public long getResult() {
        return result;
    }

    @Override
    public String toString() {
        return "ReqThree{" +
                "id='" + id + '\'' +
                ", result=" + result +
                '}';
    }
}
