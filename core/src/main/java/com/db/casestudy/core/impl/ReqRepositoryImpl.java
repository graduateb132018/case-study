package com.db.casestudy.core.impl;

import com.db.casestudy.core.api.CoreException;
import com.db.casestudy.core.api.entity.ReqOne;
import com.db.casestudy.core.api.entity.ReqThree;
import com.db.casestudy.core.api.repository.ReqRepository;
import com.db.casestudy.core.connection.Connector;
import com.db.casestudy.core.util.Log;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ReqRepositoryImpl implements ReqRepository {
    private static final Logger LOG = Log.getLogger();

    @Override
    public List<ReqOne> getReqOne() throws CoreException {
        try (Connection connection = Connector.getInstance().getConnection()) {
            try (Statement statement = connection.createStatement()) {
                try (ResultSet resultSet = statement.executeQuery("SELECT instr_id1, BUY, SELL FROM reqone;")) {
                    List<ReqOne> result = new ArrayList<>();
                    while (resultSet.next()) {
                        result.add(new ReqOne(resultSet.getString("instr_id1"),
                                resultSet.getDouble("BUY"),
                                resultSet.getDouble("SELL")
                        ));
                    }
                    return result;
                }
            }
        } catch (SQLException e) {
            LOG.log(Level.SEVERE, "Exception thrown: ", e);
            throw new CoreException(e);
        }
    }


    @Override
    public List<ReqThree> getReqThree() throws CoreException {
        try (Connection connection = Connector.getInstance().getConnection()) {
            try (Statement statement = connection.createStatement()) {
                try (ResultSet resultSet = statement.executeQuery("SELECT id, result FROM reqthree;")) {
                    List<ReqThree> result = new ArrayList<>();
                    while (resultSet.next()) {
                        result.add(new ReqThree(resultSet.getString("id"),
                                (long)resultSet.getDouble("result")
                        ));
                    }
                    return result;
                }
            }
        } catch (SQLException e) {
            LOG.log(Level.SEVERE, "Exception thrown: ", e);
            throw new CoreException(e);
        }
    }
}
