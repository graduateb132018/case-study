package com.db.casestudy.core.connection;

public interface ShutdownableDataSource extends DataSource {
    public void shutdown();
}
