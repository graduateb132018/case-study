package com.db.casestudy.core.api.repository.filter;

import com.db.casestudy.core.api.column.DealColumn;
import com.db.casestudy.core.api.entity.Deal;

public class DealFilter extends BaseFilter<Deal> {
    private final DealColumn dealColumn;

    public DealFilter(String keyWord, DealColumn dealColumn) {
        super(keyWord);
        this.dealColumn = dealColumn;
    }

    @Override
    public DealColumn getColumn() {
        return dealColumn;
    }

    @Override
    public String toString() {
        return "DealFilter{" +
                "dealColumn=" + dealColumn +
                '}';
    }
}
