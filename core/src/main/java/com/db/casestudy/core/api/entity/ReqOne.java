package com.db.casestudy.core.api.entity;

public class ReqOne {
    private final String id;
    private final double buy;
    private final double sell;

    public ReqOne(String id, double buy, double sell) {
        this.id = id;
        this.buy = buy;
        this.sell = sell;
    }

    public String getId() {
        return id;
    }

    public double getBuy() {
        return buy;
    }

    public double getSell() {
        return sell;
    }

    @Override
    public String toString() {
        return "ReqOne{" +
                "id='" + id + '\'' +
                ", buy=" + buy +
                ", sell=" + sell +
                '}';
    }
}
