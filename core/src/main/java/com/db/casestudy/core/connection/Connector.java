package com.db.casestudy.core.connection;

import com.db.casestudy.core.PropertyHolder;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public final class Connector implements ShutdownableDataSource {
    private final SimpleConnectionPool dataSource;

    private Connector(PropertyHolder propertyHolder) {
        final String url = propertyHolder.getDbUrl();
        final String user = propertyHolder.getDbUser();
        final String password = propertyHolder.getDbPassword();
        final int connectionPoolSize = propertyHolder.getConnectionPoolSize();

        dataSource = new SimpleConnectionPool(connectionPoolSize,
                () -> DriverManager.getConnection(url, user, password));
    }

    // 12.4.1, 12.4.2
    private static class ConnectorHolder {
        private static final Connector connector = new Connector(PropertyHolder.getInstance());
    }

    public static Connector getInstance() {
        return ConnectorHolder.connector;
    }

    @Override
    public Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }

    @Override
    public void shutdown() {
        dataSource.shutdown();
    }

    public boolean isConnected() {
        return dataSource.isConnected();
    }
}
