package com.db.casestudy.core.util;

import java.util.logging.Logger;

public final class Log {
    private Log() {
    }

    public static Logger getLogger() {
        return Logger.getLogger(Thread.currentThread().getStackTrace()[1].getClassName());
    }
}
