package com.db.casestudy.core.api.repository.sort;

import com.db.casestudy.core.api.entity.Entity;

public abstract class BaseSort<E extends Entity> implements Sort<E> {
    protected final SortOrder sortOrder;

    public BaseSort(SortOrder sortOrder) {
        this.sortOrder = sortOrder;
    }

    @Override
    public final SortOrder getSortOrder() {
        return sortOrder;
    }
}
