package com.db.casestudy.core.api.column;

import com.db.casestudy.core.api.entity.Deal;

import java.util.Arrays;

public enum DealColumn implements Column<Deal> {
    DEAL_ID("deal_id", "id"),
    DEAL_TYPE("deal_type", "type"),
    DEAL_TIME("deal_time", "time"),
    DEAL_AMOUNT("deal_amount", "amount"),
    DEAL_QUANTITY("deal_quantity", "quantity"),
    DEAL_INSTRUMENT_ID("deal_instrument_id", "instrumentId"),
    INSTRUMENT_NAME("instrument_name", "instrumentName"),
    DEAL_COUNTERPARTY_ID("deal_counterparty_id", "counterpartyId"),
    COUNTERPARTY_NAME("counterparty_name", "counterpartyName"),
    COUNTERPARTY_STATUS("counterparty_status", "counterpartyStatus"),
    COUNTERPARTY_DATE_REGISTERED("counterparty_date_registered", "counterpartyDateRegistered");

    private final String sqlName;
    private final String name;

    DealColumn(String sqlName, String name) {
        this.sqlName = sqlName;
        this.name = name;
    }

    @Override
    public String getSqlName() {
        return sqlName;
    }

    public String getName() {
        return name;
    }

    public static DealColumn getByName(String name) {
        return Arrays.stream(DealColumn.values())
                .filter(dealColumn -> dealColumn.name.equals(name))
                .findAny().orElseThrow(IllegalArgumentException::new);
    }
}
