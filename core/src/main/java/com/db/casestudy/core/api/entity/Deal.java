package com.db.casestudy.core.api.entity;

import java.util.Objects;

public class Deal implements Entity {

    private final int id;
    private final String time;
    private final char type; //todo: enum
    private final double amount;
    private final int quantity;
    private final Instrument instrument;
    private final Counterparty counterparty;

    public Deal(int id, String time, char type, double amount, int quantity, Instrument instrument, Counterparty counterparty) {
        this.id = id;
        this.time = time;
        this.type = type;
        this.amount = amount;
        this.quantity = quantity;
        this.instrument = instrument;
        this.counterparty = counterparty;
    }

    public int getId() {
        return id;
    }

    public String getTime() {
        return time;
    }

    public char getType() {
        return type;
    }

    public double getAmount() {
        return amount;
    }

    public int getQuantity() {
        return quantity;
    }

    public Instrument getInstrument() {
        return instrument;
    }

    public Counterparty getCounterparty() {
        return counterparty;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Deal deal = (Deal) o;
        return id == deal.id &&
                type == deal.type &&
                Double.compare(deal.amount, amount) == 0 &&
                quantity == deal.quantity &&
                Objects.equals(time, deal.time) &&
                Objects.equals(instrument, deal.instrument) &&
                Objects.equals(counterparty, deal.counterparty);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, time, type, amount, quantity, instrument, counterparty);
    }

    @Override
    public String toString() {
        return "Deal{" +
                "id=" + id +
                ", time='" + time + '\'' +
                ", type=" + type +
                ", amount=" + amount +
                ", quantity=" + quantity +
                ", instrument=" + instrument +
                ", counterparty=" + counterparty +
                '}';
    }
}
