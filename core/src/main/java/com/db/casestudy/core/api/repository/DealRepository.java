package com.db.casestudy.core.api.repository;

import com.db.casestudy.core.api.CoreException;
import com.db.casestudy.core.api.entity.CounterpartyNameDealTypeCount;
import com.db.casestudy.core.api.entity.Deal;
import com.db.casestudy.core.api.repository.request.FilteredSortedPageRequest;

import java.util.List;

public interface DealRepository extends BasicRepository<Deal>, FilterableSortablePaginationRepository<Deal> {
    @Override
    List<Deal> getAll() throws CoreException;

    @Override
    List<Deal> getPage(FilteredSortedPageRequest<Deal> filteredSortedPageRequest) throws CoreException;

    @Override
    int getTotalRows(FilteredSortedPageRequest<Deal> filteredSortedPageRequest) throws CoreException;

    List<CounterpartyNameDealTypeCount> getCounterpartyNameDealTypeCount() throws CoreException;
}
