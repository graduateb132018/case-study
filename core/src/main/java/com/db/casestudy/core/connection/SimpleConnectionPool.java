package com.db.casestudy.core.connection;

import com.db.casestudy.core.util.Log;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SimpleConnectionPool implements ShutdownableDataSource {
    private static final Logger LOG = Log.getLogger();
    private static final int ALLOCATOR_RETRY_PERIOD = 1_000;

    private final DataSource internalDataSource;
    private final Queue<Connection> pool;
    private final Executor conditionSignallingExecutor;
    private final Lock lock;
    private final Condition connectionConsumed;
    private volatile boolean connected;

    public SimpleConnectionPool(int size, DataSource internalDataSource) {
        this.internalDataSource = internalDataSource;
        pool = new ConcurrentLinkedQueue<>();
        lock = new ReentrantLock();
        connectionConsumed = lock.newCondition();
        conditionSignallingExecutor = Executors.newCachedThreadPool(r -> new Thread(r, "SimpleConnectionPoolConditionSignallingThread"));
        connected = false;
        startConnectionAllocator(
                internalDataSource, size,
                pool, lock, connectionConsumed,
                s -> connected = s);
    }

    private static void startConnectionAllocator(final DataSource internalDataSource,
                                                 final int size,
                                                 final Queue<Connection> pool,
                                                 final Lock lock,
                                                 final Condition connectionConsumed,
                                                 Consumer<Boolean> statusConsumer) {
        new Thread(() -> {
            lock.lock();
            try {
                int lastActualSize = pool.size();
                for (; ; ) {
                    do {
                        for (int i = lastActualSize; i < size; i++) {
                            try {
                                pool.add(internalDataSource.getConnection());
                                statusConsumer.accept(true);
                                LOG.info("SimpleConnectionPoolAllocatorThread - connection added");
                            } catch (SQLException e) {
                                LOG.log(Level.SEVERE, "Exception thrown: ", e);
                                statusConsumer.accept(false);
                                try {
                                    Thread.sleep(ALLOCATOR_RETRY_PERIOD);
                                } catch (InterruptedException ignored) {
                                }
                            }
                        }
                    } while ((lastActualSize = pool.size()) < size);

                    do {
                        try {
                            connectionConsumed.await();
                        } catch (InterruptedException e) {
                            LOG.log(Level.SEVERE, "Exception thrown: ", e);
                        }
                    } while ((lastActualSize = pool.size()) >= size);

                    LOG.info("SimpleConnectionPoolAllocatorThread - connectionConsumed");
                }
            } finally {
                lock.unlock();
            }
        }, "SimpleConnectionPoolAllocatorThread").start();
    }

    @Override
    public Connection getConnection() throws SQLException {
        Connection connection = pool.poll();
        if (connection == null) {
            LOG.info("WARN: pool is empty, creating Connection directly");
            try {
                Connection c = internalDataSource.getConnection();
                connected = true;
                return c;
            } catch (SQLException e) {
                connected = false;
                throw e;
            }
        }
        //todo: validate connection

        conditionSignallingExecutor.execute(() -> {
            lock.lock();
            try {
                connectionConsumed.signalAll();
                LOG.info("ConnectionConsumedSignallingExecutor - signal all");
            } finally {
                lock.unlock();
            }
        });

        return connection;
    }

    @Override
    public void shutdown() {
        //todo: do proper shutdown
        for (Connection connection : pool) {
            try {
                connection.close();
            } catch (SQLException e) {
                LOG.log(Level.SEVERE, "Exception thrown: ", e);
            }
        }
    }

    public boolean isConnected() {
        return connected;
    }
}
