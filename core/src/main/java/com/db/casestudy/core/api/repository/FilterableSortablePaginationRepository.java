package com.db.casestudy.core.api.repository;

import com.db.casestudy.core.api.CoreException;
import com.db.casestudy.core.api.entity.Entity;
import com.db.casestudy.core.api.repository.request.FilteredSortedPageRequest;

import java.util.List;

public interface FilterableSortablePaginationRepository<E extends Entity> {
    List<E> getPage(FilteredSortedPageRequest<E> filteredSortedPageRequest) throws CoreException;

    int getTotalRows(FilteredSortedPageRequest<E> filteredSortedPageRequest) throws CoreException;
}
