package com.db.casestudy.core.api.entity;

import java.time.LocalDateTime;
import java.util.Objects;

public class Counterparty {
    private final int id;
    private final String name;
    private final char status; //todo: enum
    private final LocalDateTime dateRegistered;

    public Counterparty(int id, String name, char status, LocalDateTime dateRegistered) {
        this.id = id;
        this.name = name;
        this.status = status;
        this.dateRegistered = dateRegistered;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public char getStatus() {
        return status;
    }

    public LocalDateTime getDateRegistered() {
        return dateRegistered;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Counterparty that = (Counterparty) o;
        return id == that.id &&
                Objects.equals(name, that.name) &&
                Objects.equals(status, that.status) &&
                Objects.equals(dateRegistered, that.dateRegistered);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, status, dateRegistered);
    }

    @Override
    public String toString() {
        return "Counterparty{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", status='" + status + '\'' +
                ", dateRegistered=" + dateRegistered +
                '}';
    }
}
