package com.db.casestudy.core.connection;

import java.sql.Connection;
import java.sql.SQLException;

@FunctionalInterface
public interface DataSource {
    Connection getConnection() throws SQLException;
}
