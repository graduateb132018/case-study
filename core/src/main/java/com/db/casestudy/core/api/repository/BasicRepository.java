package com.db.casestudy.core.api.repository;

import com.db.casestudy.core.api.CoreException;
import com.db.casestudy.core.api.entity.Entity;

import java.util.List;

public interface BasicRepository<E extends Entity> {
    List<E> getAll() throws CoreException;
}
