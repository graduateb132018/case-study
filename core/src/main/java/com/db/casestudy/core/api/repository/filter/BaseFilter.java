package com.db.casestudy.core.api.repository.filter;

import com.db.casestudy.core.api.entity.Entity;

public abstract class BaseFilter<E extends Entity> implements Filter<E> {
    private final String keyWord;

    protected BaseFilter(String keyWord) {
        this.keyWord = keyWord;
    }

    @Override
    public String getKeyWord() {
        return keyWord;
    }
}
