package com.db.casestudy.core.impl;

import com.db.casestudy.core.api.entity.Deal;
import com.db.casestudy.core.api.repository.filter.Filter;
import com.db.casestudy.core.api.repository.request.FilteredSortedPageRequest;
import com.db.casestudy.core.api.repository.sort.Sort;
import com.db.casestudy.core.util.Log;

import java.util.logging.Logger;

public class DealRepositoryQueryConstructor {
    private static final Logger LOG = Log.getLogger();

    private static final String SELECT_ALL_BASE_QUERY = "SELECT d.deal_id, d.deal_time, d.deal_type, d.deal_amount, d.deal_quantity, " +
            "d.deal_instrument_id, d.deal_counterparty_id, " +
            "i.instrument_name, " +
            "c.counterparty_name, c.counterparty_status, c.counterparty_date_registered ";

    private static final String SELECT_QUERY_POSTFIX = "FROM deal d " +
            "JOIN counterparty c ON d.deal_counterparty_id = c.counterparty_id " +
            "JOIN instrument i ON d.deal_instrument_id = i.instrument_id";

    public static final String SELECT_ALL_QUERY = SELECT_ALL_BASE_QUERY + SELECT_QUERY_POSTFIX + ";";

    private final FilteredSortedPageRequest<Deal> request;

    public DealRepositoryQueryConstructor(FilteredSortedPageRequest<Deal> dealFilteredSortedPageRequest) {
        this.request = dealFilteredSortedPageRequest;
    }

    public String constructCountSqlForPreparedStatement() {
        StringBuilder query = new StringBuilder("SELECT COUNT(d.deal_id) as C ");

        getAddRequestMatchingQuery(query);

        String s = query.append(";").toString();
        LOG.info(s);
        return s;
    }

    public String constructPageSqlForPreparedStatement() {
        StringBuilder query = new StringBuilder(SELECT_ALL_BASE_QUERY);

        getAddRequestMatchingQuery(query);

        query.append(" LIMIT ").append(request.getEntryNumber() * (request.getPageNumber() - 1))
                .append(", ").append(request.getEntryNumber()).append(";");

        String s = query.toString();
        LOG.info(s);
        return s;
    }

    private void getAddRequestMatchingQuery(StringBuilder query) {
        query.append(SELECT_QUERY_POSTFIX);

        final int amountOfFilters = request.getFilters().size();
        if (amountOfFilters > 0) {
            query.append(" WHERE");
            for (int i = 0; i < amountOfFilters; i++) {
                if (i > 0) {
                    query.append(" AND");
                }
                Filter<Deal> filter = request.getFilters().get(i);
                query.append(" ").append(filter.getColumn().getSqlName()).append(" = ").append("?"/*filter.getKeyWord()*/).append("");
            }
        }

        final int amountOfSorts = request.getSorts().size();
        if (amountOfSorts > 0) {
            query.append(" ORDER BY");
            for (int i = 0; i < amountOfSorts; i++) {
                if (i > 0) {
                    query.append(",");
                }
                Sort<Deal> sort = request.getSorts().get(i);
                query.append(" ").append(sort.getColumn().getSqlName()).append(" ").append(sort.getSortOrder().name());
            }
        }
    }
}
