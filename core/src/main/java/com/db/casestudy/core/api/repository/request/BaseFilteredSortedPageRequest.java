package com.db.casestudy.core.api.repository.request;

import com.db.casestudy.core.api.entity.Entity;
import com.db.casestudy.core.api.repository.filter.Filter;
import com.db.casestudy.core.api.repository.sort.Sort;

import java.util.List;

public abstract class BaseFilteredSortedPageRequest<E extends Entity> implements FilteredSortedPageRequest<E> {
    private final int pageNumber;
    private final int entryNumber;
    private final List<Sort<E>> sorts;
    private final List<Filter<E>> filters;

    public BaseFilteredSortedPageRequest(int pageNumber, int entryNumber, List<Sort<E>> sorts, List<Filter<E>> filters) {
        this.pageNumber = pageNumber;
        this.entryNumber = entryNumber;
        this.sorts = sorts;
        this.filters = filters;
    }

    @Override
    public int getPageNumber() {
        return pageNumber;
    }

    @Override
    public int getEntryNumber() {
        return entryNumber;
    }

    @Override
    public List<Sort<E>> getSorts() {
        return sorts;
    }

    @Override
    public List<Filter<E>> getFilters() {
        return filters;
    }

    @Override
    public String toString() {
        return "BaseFilteredSortedPageRequest{" +
                "pageNumber=" + pageNumber +
                ", entryNumber=" + entryNumber +
                ", sorts=" + sorts +
                ", filters=" + filters +
                '}';
    }
}
