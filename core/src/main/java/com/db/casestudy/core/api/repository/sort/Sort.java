package com.db.casestudy.core.api.repository.sort;

import com.db.casestudy.core.api.column.Column;
import com.db.casestudy.core.api.entity.Entity;

public interface Sort<E extends Entity> {
    Column<E> getColumn();

    SortOrder getSortOrder();
}
