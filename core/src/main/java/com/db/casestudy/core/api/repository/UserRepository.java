package com.db.casestudy.core.api.repository;

import com.db.casestudy.core.api.CoreException;
import com.db.casestudy.core.api.entity.User;

public interface UserRepository {
    User getUserByUsername(String username) throws CoreException;
}
