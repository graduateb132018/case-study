package com.db.casestudy.core.api.repository.sort;

import com.db.casestudy.core.api.column.DealColumn;
import com.db.casestudy.core.api.entity.Deal;

public class DealSort extends BaseSort<Deal> {
    private final DealColumn dealColumn;

    public DealSort(SortOrder sortOrder, DealColumn dealColumn) {
        super(sortOrder);
        this.dealColumn = dealColumn;
    }

    @Override
    public DealColumn getColumn() {
        return dealColumn;
    }

    @Override
    public String toString() {
        return "DealSort{" +
                "dealColumn=" + dealColumn +
                ", sortOrder=" + sortOrder +
                '}';
    }
}
