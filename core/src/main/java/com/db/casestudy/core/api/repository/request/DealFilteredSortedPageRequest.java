package com.db.casestudy.core.api.repository.request;

import com.db.casestudy.core.api.entity.Deal;
import com.db.casestudy.core.api.repository.filter.Filter;
import com.db.casestudy.core.api.repository.sort.Sort;

import java.util.List;

public class DealFilteredSortedPageRequest extends BaseFilteredSortedPageRequest<Deal> {
    public DealFilteredSortedPageRequest(int pageNumber, int entryNumber, List<Sort<Deal>> sorts, List<Filter<Deal>> filters) {
        super(pageNumber, entryNumber, sorts, filters);
    }
}
