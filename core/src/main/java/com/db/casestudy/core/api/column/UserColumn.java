package com.db.casestudy.core.api.column;

import com.db.casestudy.core.api.entity.User;

public enum UserColumn implements Column<User> {
    USERNAME("user_id"), PASSWORD("user_pwd");

    private final String sqlName;

    UserColumn(String sqlName) {
        this.sqlName = sqlName;
    }

    @Override
    public String getSqlName() {
        return null;
    }
}
