package com.db.casestudy.core.api.column;

import com.db.casestudy.core.api.entity.Entity;

public interface Column<E extends Entity> {
    String getSqlName();
}
