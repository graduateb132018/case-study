package com.db.casestudy.core.api;

public class CoreException extends Exception{
    public CoreException(Throwable cause) {
        super(cause);
    }

    public CoreException(String message, Throwable cause) {
        super(message, cause);
    }
}
