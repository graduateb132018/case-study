package com.db.casestudy.core;

import com.db.casestudy.core.util.Log;

import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class PropertyHolder {
    private static final Logger LOG = Log.getLogger();

    private PropertyHolder(String path) {
        properties = new Properties();
        try {
            InputStream stream = getClass().getClassLoader().getResourceAsStream(path);
            properties.load(stream);
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Exception thrown: ", e);
        }
    }

    private final Properties properties;

    // 12.4.1, 12.4.2
    private static class PropertyHolderHolder {
        private static final String path = "configuration.properties";
        private static final PropertyHolder PropertyHolder = new PropertyHolder(path);
    }

    public static PropertyHolder getInstance() {
        return PropertyHolderHolder.PropertyHolder;
    }

    public Properties getProperties() {
        return new Properties(properties);
    }

    public String getDbUrl() {
        return properties.getProperty("db.url");
    }

    public String getDbUser() {
        return properties.getProperty("db.user");
    }

    public String getDbPassword() {
        return properties.getProperty("db.password");
    }

    public int getConnectionPoolSize() {
        return Integer.parseInt(properties.getProperty("cp.size"));
    }
}
