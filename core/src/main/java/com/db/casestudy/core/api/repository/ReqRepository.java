package com.db.casestudy.core.api.repository;

import com.db.casestudy.core.api.CoreException;
import com.db.casestudy.core.api.entity.ReqOne;
import com.db.casestudy.core.api.entity.ReqThree;

import java.util.List;

public interface ReqRepository {
    List<ReqOne> getReqOne() throws CoreException;
    List<ReqThree> getReqThree() throws CoreException;
}
