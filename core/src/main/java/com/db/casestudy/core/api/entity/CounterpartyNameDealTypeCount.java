package com.db.casestudy.core.api.entity;

public class CounterpartyNameDealTypeCount {
    private final long count;
    private final String counterpartyName;
    private final char dealType;

    public CounterpartyNameDealTypeCount(long count, String counterpartyName, char dealType) {
        this.count = count;
        this.counterpartyName = counterpartyName;
        this.dealType = dealType;
    }

    public long getCount() {
        return count;
    }

    public String getCounterpartyName() {
        return counterpartyName;
    }

    public char getDealType() {
        return dealType;
    }

    @Override
    public String toString() {
        return "CounterpartyNameDealTypeCount{" +
                "count=" + count +
                ", counterpartyName='" + counterpartyName + '\'' +
                ", dealType=" + dealType +
                '}';
    }
}
