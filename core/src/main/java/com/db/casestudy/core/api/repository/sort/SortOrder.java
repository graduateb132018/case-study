package com.db.casestudy.core.api.repository.sort;

public enum SortOrder {
    ASC, DESC;
}
