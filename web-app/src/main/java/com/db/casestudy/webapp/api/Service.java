package com.db.casestudy.webapp.api;

import com.db.casestudy.core.util.Log;
import com.db.casestudy.webapp.controller.ControllerException;
import com.db.casestudy.webapp.controller.DealsController;
import com.db.casestudy.webapp.controller.LoginController;
import com.db.casestudy.webapp.dto.Credentials;
import com.db.casestudy.webapp.dto.PageDto;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Path("/api")
public class Service {
    private static final Logger LOG = Log.getLogger();

    public static final String SESSION_LOGIN_ATTRIBUTE_KEY = "LOGIN";

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/raw-data")
    public Response getRawData(@QueryParam("pageNumber") Integer pageNumber,
                               @QueryParam("entryNumber") Integer entryNumber,
                               @QueryParam("filterKeyWords") List<String> filterKeyWords,
                               @QueryParam("filterColumns") List<String> filterColumns,
                               @QueryParam("sortOrders") List<String> sortOrders,
                               @QueryParam("sortColumns") List<String> sortColumn) {
        try {
            PageDto page = new DealsController()
                    .getPage(pageNumber, entryNumber, filterKeyWords, filterColumns, sortOrders, sortColumn);
            return Response.ok(page, MediaType.APPLICATION_JSON_TYPE).build();
        } catch (ControllerException e) {
            LOG.log(Level.SEVERE, "Exception thrown: ", e);
            return Response.serverError().build();
        }
    }

    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Path("/login")
    public Response login(Credentials credentials, @Context HttpServletRequest request) {
        try {
            if (new LoginController().check(credentials)) {
                request.getSession().setAttribute(SESSION_LOGIN_ATTRIBUTE_KEY, credentials.getUsername());
                return Response.ok("true").build();
            } else {
                return Response.ok("false").build();
            }
        } catch (ControllerException e) {
            LOG.log(Level.SEVERE, "Exception thrown: ", e);
            return Response.serverError().build();
        }
    }

    @GET
    @Path("/logout")
    public Response logout(@Context HttpServletRequest request) {
        request.getSession().setAttribute(SESSION_LOGIN_ATTRIBUTE_KEY, null);
        return Response.ok().build();
    }

    @GET
    @Path("/username")
    public Response getUsername(@Context HttpServletRequest request) {
        return Response.ok(request.getSession().getAttribute(SESSION_LOGIN_ATTRIBUTE_KEY)).build();
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/counterpartyname-dealtype-count")
    public Response getCounterpartyNameDealTypeCount() {
        try {
            return Response.ok(new DealsController().getCounterpartyNameDealTypeCount()).build();
        } catch (ControllerException e) {
            LOG.log(Level.SEVERE, "Exception thrown: ", e);
            return Response.serverError().build();
        }
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/deals")
    public Response getDeals() {
        try {
            return Response.ok(new DealsController().getDeals()).build();
        } catch (ControllerException e) {
            LOG.log(Level.SEVERE, "Exception thrown: ", e);
            return Response.serverError().build();
        }
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/deals-simple")
    public Response getDealsSimple() {
        try {
            return Response.ok(new DealsController().getSimpleDeals()).build();
        } catch (ControllerException e) {
            LOG.log(Level.SEVERE, "Exception thrown: ", e);
            return Response.serverError().build();
        }
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/deals-time-quantity-sum")
    public Response getDealsTimeQuantitySum() {
        try {
            return Response.ok(new DealsController().getDealsTimeQuantitySum()).build();
        } catch (ControllerException e) {
            LOG.log(Level.SEVERE, "Exception thrown: ", e);
            return Response.serverError().build();
        }
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/deals-count-by-time-type")
    public Response getCountByDealsTimeType() {
        try {
            return Response.ok(new DealsController().getCountByDealsTimeType()).build();
        } catch (ControllerException e) {
            LOG.log(Level.SEVERE, "Exception thrown: ", e);
            return Response.serverError().build();
        }
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/req-1")
    public Response getReqOne() {
        try {
            return Response.ok(new DealsController().getReqOne()).build();
        } catch (ControllerException e) {
            LOG.log(Level.SEVERE, "Exception thrown: ", e);
            return Response.serverError().build();
        }
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/req-3")
    public Response getReqThree() {
        try {
            return Response.ok(new DealsController().getReqThree()).build();
        } catch (ControllerException e) {
            LOG.log(Level.SEVERE, "Exception thrown: ", e);
            return Response.serverError().build();
        }
    }
}
