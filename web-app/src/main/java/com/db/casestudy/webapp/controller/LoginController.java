package com.db.casestudy.webapp.controller;

import com.db.casestudy.core.api.CoreException;
import com.db.casestudy.core.api.entity.User;
import com.db.casestudy.core.api.repository.UserRepository;
import com.db.casestudy.core.impl.UserRepositoryImpl;
import com.db.casestudy.core.util.Log;
import com.db.casestudy.webapp.dto.Credentials;

import java.util.logging.Level;
import java.util.logging.Logger;

public class LoginController {
    private static final Logger LOG = Log.getLogger();

    private final UserRepository userRepository;

    public LoginController() {
        userRepository = new UserRepositoryImpl();
    }

    public boolean check(Credentials credentials) throws ControllerException {
        try {
            User user = userRepository.getUserByUsername(credentials.getUsername());
            return user != null && user.getPassword() != null && user.getPassword().equals(credentials.getPassword());
        } catch (CoreException e) {
            LOG.log(Level.SEVERE, "Exception thrown: ", e);
            throw new ControllerException(e);
        }

    }
}
