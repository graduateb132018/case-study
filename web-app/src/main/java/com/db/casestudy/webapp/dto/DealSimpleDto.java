package com.db.casestudy.webapp.dto;

import com.db.casestudy.core.api.entity.Deal;

public class DealSimpleDto {
    private final char type;
    private final double amount;
    private final int quantity;
    private final String instrumentName;
    private final String counterpartyName;

    public DealSimpleDto(Deal deal) {
        this.type = deal.getType();
        this.amount = deal.getAmount();
        this.quantity = deal.getQuantity();
        this.instrumentName = deal.getInstrument().getName();
        this.counterpartyName = deal.getCounterparty().getName();
    }

    public char getType() {
        return type;
    }

    public double getAmount() {
        return amount;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getInstrumentName() {
        return instrumentName;
    }

    public String getCounterpartyName() {
        return counterpartyName;
    }

    @Override
    public String toString() {
        return "DealSimpleDto{" +
                "type=" + type +
                ", amount=" + amount +
                ", quantity=" + quantity +
                ", instrumentName='" + instrumentName + '\'' +
                ", counterpartyName='" + counterpartyName + '\'' +
                '}';
    }
}
