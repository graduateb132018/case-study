package com.db.casestudy.webapp.controller;

import com.db.casestudy.core.api.CoreException;
import com.db.casestudy.core.api.column.DealColumn;
import com.db.casestudy.core.api.entity.CounterpartyNameDealTypeCount;
import com.db.casestudy.core.api.entity.Deal;
import com.db.casestudy.core.api.entity.ReqOne;
import com.db.casestudy.core.api.entity.ReqThree;
import com.db.casestudy.core.api.repository.DealRepository;
import com.db.casestudy.core.api.repository.filter.DealFilter;
import com.db.casestudy.core.api.repository.filter.Filter;
import com.db.casestudy.core.api.repository.request.DealFilteredSortedPageRequest;
import com.db.casestudy.core.api.repository.sort.DealSort;
import com.db.casestudy.core.api.repository.sort.Sort;
import com.db.casestudy.core.api.repository.sort.SortOrder;
import com.db.casestudy.core.impl.DealRepositoryImpl;
import com.db.casestudy.core.impl.ReqRepositoryImpl;
import com.db.casestudy.core.util.Log;
import com.db.casestudy.webapp.dto.DealDto;
import com.db.casestudy.webapp.dto.DealSimpleDto;
import com.db.casestudy.webapp.dto.PageDto;
import com.db.casestudy.webapp.dto.TimeTypeDto;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class DealsController {
    private static final Logger LOG = Log.getLogger();
    private final DealRepository dealRepository;
    private static final int DEFAULT_ENTRY_NUMBER = 10;

    public DealsController() {
        dealRepository = new DealRepositoryImpl();
    }

    public PageDto getPage(Integer pageNumber, Integer entryNumber,
                           List<String> filterKeyWords, List<String> filterColumns,
                           List<String> sortOrders, List<String> sortColumn) throws ControllerException {
        try {
            if (pageNumber == null) {
                pageNumber = 1;
            }

            if (entryNumber == null) {
                entryNumber = DEFAULT_ENTRY_NUMBER;
            }

            List<Filter<Deal>> dealFilters = new ArrayList<>();
            if (!filterKeyWords.isEmpty() && filterKeyWords.size() == filterColumns.size()) {
                for (int i = 0; i < filterKeyWords.size(); i++) {
                    dealFilters.add(new DealFilter(filterKeyWords.get(i), DealColumn.getByName(filterColumns.get(i))));
                }
            }

            List<Sort<Deal>> dealSorts = new ArrayList<>();
            if (!sortOrders.isEmpty() && sortOrders.size() == sortColumn.size()) {
                for (int i = 0; i < sortOrders.size(); i++) {
                    dealSorts.add(new DealSort(SortOrder.valueOf(sortOrders.get(i)), DealColumn.getByName(sortColumn.get(i))));
                }
            }

            DealFilteredSortedPageRequest dealFilteredSortedPageRequest =
                    new DealFilteredSortedPageRequest(pageNumber, entryNumber, dealSorts, dealFilters);

            List<DealDto> deals = dealRepository.getPage(dealFilteredSortedPageRequest)
                    .stream().map(DealDto::new).collect(Collectors.toList());
            int totalRows = dealRepository.getTotalRows(dealFilteredSortedPageRequest);
            return new PageDto(deals, totalRows);
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Exception thrown: ", e);
            throw new ControllerException(e);
        }
    }

    public List<CounterpartyNameDealTypeCount> getCounterpartyNameDealTypeCount() throws ControllerException {
        try {
            return new DealRepositoryImpl().getCounterpartyNameDealTypeCount();
        } catch (CoreException e) {
            LOG.log(Level.SEVERE, "Exception thrown: ", e);
            throw new ControllerException(e);
        }
    }

    public List<DealDto> getDeals() throws ControllerException {
        try {
            return new DealRepositoryImpl().getAll().stream().map(DealDto::new).collect(Collectors.toList());
        } catch (CoreException e) {
            LOG.log(Level.SEVERE, "Exception thrown: ", e);
            throw new ControllerException(e);
        }
    }

    public List<DealSimpleDto> getSimpleDeals() throws ControllerException {
        try {
            return new DealRepositoryImpl().getAll().stream().map(DealSimpleDto::new).collect(Collectors.toList());
        } catch (CoreException e) {
            LOG.log(Level.SEVERE, "Exception thrown: ", e);
            throw new ControllerException(e);
        }
    }

    private final static Pattern MM_SS_PATTERN = Pattern.compile("\\d\\d:(\\d\\d:\\d\\d)(\\.\\d\\d\\d)?$");

    private static String timeStringToMinutesSecondsFormat(Deal d) {
        Matcher matcher = MM_SS_PATTERN.matcher(d.getTime());
        return matcher.find() ? matcher.group(1) : null;
    }

    public Map<String, Integer> getDealsTimeQuantitySum() throws ControllerException {
        try {
            return new DealRepositoryImpl().getAll().stream()
                    .collect(Collectors.groupingBy(DealsController::timeStringToMinutesSecondsFormat))
                    .entrySet().stream()
                    .map(e -> new AbstractMap.SimpleEntry<>(e.getKey(),
                            e.getValue().stream().mapToInt(Deal::getQuantity).sum()))
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        } catch (CoreException e) {
            LOG.log(Level.SEVERE, "Exception thrown: ", e);
            throw new ControllerException(e);
        }
    }

    public Map<TimeTypeDto, Integer> getCountByDealsTimeType() throws ControllerException {
        try {
            return new DealRepositoryImpl().getAll().stream()
                    .collect(Collectors.groupingBy(deal
                            -> new TimeTypeDto(timeStringToMinutesSecondsFormat(deal), deal.getType())))
                    .entrySet().stream()
                    .map(e -> new AbstractMap.SimpleEntry<>(e.getKey(), e.getValue().size()))
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        } catch (CoreException e) {
            LOG.log(Level.SEVERE, "Exception thrown: ", e);
            throw new ControllerException(e);
        }
    }

    public List<ReqOne> getReqOne() throws ControllerException {
        try {
            return new ReqRepositoryImpl().getReqOne();
        } catch (CoreException e) {
            LOG.log(Level.SEVERE, "Exception thrown: ", e);
            throw new ControllerException(e);
        }
    }

    public List<ReqThree> getReqThree() throws ControllerException {
        try {
            return new ReqRepositoryImpl().getReqThree();
        } catch (CoreException e) {
            LOG.log(Level.SEVERE, "Exception thrown: ", e);
            throw new ControllerException(e);
        }
    }
}
