package com.db.casestudy.webapp.dto;

import java.util.Objects;

public class TimeTypeDto {
    private final String time;
    private final char type;

    public TimeTypeDto(String time, char type) {
        this.time = time;
        this.type = type;
    }

    public String getTime() {
        return time;
    }

    public char getType() {
        return type;
    }

    @Override
    public String toString() {
        return "TimeTypeDto{" +
                "time='" + time + '\'' +
                ", type=" + type +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TimeTypeDto that = (TimeTypeDto) o;
        return type == that.type &&
                Objects.equals(time, that.time);
    }

    @Override
    public int hashCode() {
        return Objects.hash(time, type);
    }
}
