package com.db.casestudy.webapp.dto;

import com.db.casestudy.core.api.entity.Deal;

import java.time.format.DateTimeFormatter;

public class DealDto {
    private final int id;
    private final char type;
    private final String time;
    private final double amount;
    private final int quantity;
    private final String instrumentName;
    private final String counterpartyName;
    private final char counterpartyStatus;
    private final String counterpartyDateRegistered;

    public DealDto(Deal deal) {
        this.id = deal.getId();
        this.type = deal.getType();
        this.time = deal.getTime();
        this.amount = deal.getAmount();
        this.quantity = deal.getQuantity();
        this.instrumentName = deal.getInstrument().getName();
        this.counterpartyName = deal.getCounterparty().getName();
        this.counterpartyStatus = deal.getCounterparty().getStatus();
        this.counterpartyDateRegistered = deal.getCounterparty().getDateRegistered().format(DateTimeFormatter.ISO_DATE);
    }

    public int getId() {
        return id;
    }

    public char getType() {
        return type;
    }

    public String getTime() {
        return time;
    }

    public double getAmount() {
        return amount;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getInstrumentName() {
        return instrumentName;
    }

    public String getCounterpartyName() {
        return counterpartyName;
    }

    public char getCounterpartyStatus() {
        return counterpartyStatus;
    }

    public String getCounterpartyDateRegistered() {
        return counterpartyDateRegistered;
    }
}
