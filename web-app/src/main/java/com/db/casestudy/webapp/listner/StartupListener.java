package com.db.casestudy.webapp.listner;

import com.db.casestudy.core.connection.Connector;
import com.db.casestudy.core.util.Log;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.util.logging.Logger;

@WebListener
public class StartupListener implements ServletContextListener {
    private static final Logger LOG = Log.getLogger();

    @Override
    public void contextInitialized(ServletContextEvent event) {
        LOG.info("contextInitialized");
        LOG.info(Connector.getInstance().toString());
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        LOG.info("contextDestroyed");
        Connector.getInstance().shutdown();
    }
}