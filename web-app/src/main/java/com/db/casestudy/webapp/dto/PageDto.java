package com.db.casestudy.webapp.dto;

import java.util.List;

public class PageDto {
    private final List<DealDto> deals;
    private final int totalRows;

    public PageDto(List<DealDto> deals, int totalRows) {
        this.deals = deals;
        this.totalRows = totalRows;
    }

    public List<DealDto> getDeals() {
        return deals;
    }

    public int getTotalRows() {
        return totalRows;
    }
}
