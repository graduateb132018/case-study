<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.db.casestudy.core.connection.Connector" %>
<html>
<head>
    <title>Login page</title>
    <!-- local imports -->
    <!-- bootstrap imports -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src='d3_script.js'></script>
    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
    <script type="text/javascript" src="loginscript.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css"
          integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="main.css">
</head>

<body class="mybody">

<div id="login">
    <div id="logo-login" class="container">
        <!-- container for logo -->
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <img src="Deutsche_Bank_logo.png"/>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>

    <div id="login-form" class="container" style="margin-top:5%; display:block;">
        <!-- container to hide after successful login -->
        <div class="row">
            <!-- welcome text row -->
            <div class="col-md-2"></div>
            <div class="col-md-8 center"><p style="text-align:center"> Welcome to Team B1.3's data analysis center! To
                view statistics and trading data please log in first.</p></div>
            <div class="col-md-2"></div>
        </div>
        <div class="row">
            <!-- usrname & pwd row -->
            <div class="col-md-4"></div>
            <div class="col-md-4 center">
                <form name="login">
                    <input type="text" class="form-control" id="userid" placeholder="Username"/> <br>
                    <input type="password" class="form-control" id="pswrd" placeholder="Password"/> <br>
                </form>
            </div>
            <div class="col-md-4"></div>
        </div>

        <div class="row">
            <!-- submit button row -->
            <div class="col-md-5"></div>
            <div class="col-md-2 center">
                <input type="button" class="btn btn-default" value="Login" onclick="clickLogin();" style="width:100%"/>
            </div>
            <div class="col-md-5"></div>
        </div>
    </div>
</div>

<!--  -->
<!--  -->

<div id="actualsite" style="display:none;">
    <!-- container for actual site hidden until successful submit -->
    <div id="visual">
    </div>
</div>

<div id="connection" style="display:block">
      <%= Connector.getInstance().isConnected() ? "<b style='color:green'>Successfully connected to database!</b>" : "<b style='color:red'>Unable to connect to database</b>" %>
</div>

</body>
</html>
