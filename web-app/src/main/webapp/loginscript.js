let LOGIN = "login";
let ACTUALSITE = "actualsite";
let ACTUALSITE_HTML = "site.html";
let LOGIN_HTML = "login.html";
let LOGIN_MESSAGE = "Successful login!";
let FAIL_LOGIN_MESSAGE = "Unsuccessful login! Incorrect credentials";
// raw data defaults
let RAWDATA_URL = "rws/api/raw-data";
let LOGIN_URL = "rws/api/login";
let LOGOUT_URL = "rws/api/logout";
let USER_URL = "rws/api/username";
let columnKeywordMap = new Map(); // col to keyword
let columnSortMap = new Map();  // col to sort order
let PAGE_NO = 1;
let ENTRY_PER_PAGE = 10;
let RAWDATA_COLS = ['id', 'time', 'type', 'amount', 'quantity', 'instrumentName', 'counterpartyName', 'counterpartyStatus'
    , 'counterpartyDateRegistered'];
let NUM_ROWS = 2000;
let NUM_PAGES = Math.round(NUM_ROWS / ENTRY_PER_PAGE);
let INSTRUMENTS = ["Astronomica","Borealis","Celestial","Deuteronic","Eclipse","Floral","Galactia","Heliosphere","Interstella","Jupiter","Koronis","Lunatic",""];


window.onload = enableEnter();
setUpFilterMap();

credProbe();

function clickLogin() {
    id = document.getElementById("userid").value;
    pw = document.getElementById("pswrd").value;

    $.ajax({
        type: "POST",
        url: LOGIN_URL,
        data: JSON.stringify({
            "username": id,
            "password": pw
        }),
        contentType: "application/json",
        success: function (data) {
            if (data == 'true'){
                alert(LOGIN_MESSAGE);
                clickLoginHelper();
                switchVisibility('connection');
                disableEnter();
            }
            else {
                alert(FAIL_LOGIN_MESSAGE);
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(FAIL_LOGIN_MESSAGE);
        }
    });
}

function clickLogout() {
    $.get(LOGOUT_URL, function (data) {
        alert("logout");
        clearBox(ACTUALSITE);
        switchVisibility(ACTUALSITE);
        switchVisibility('connection');
        $("#" + LOGIN).load(LOGIN_HTML);
        switchVisibility(LOGIN);
        //enableEnter();
        //empty out states
        columnKeywordMap.clear();
        columnSortMap.clear();
        PAGE_NO = 1;
        ENTRY_PER_PAGE = 10;
    });

}


function switchVisibility(i) {
    var x = document.getElementById(i);
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}

function clearBox(elementID) {
    var e = document.getElementById(elementID);
    e.innerHTML = "";

}

function setUpFilterMap() {
    for (i = 0; i < RAWDATA_COLS.length; i++) {
        columnKeywordMap.set(RAWDATA_COLS[i],null);
        columnSortMap.set(RAWDATA_COLS[i],null);
    }
}

function sortOrderClick(name) {
// automatically run with name of column inserted from html

    var sortOrder = columnSortMap.get(name);
    var x = document.getElementById(name);
    if (sortOrder == 'ASC') {
        columnSortMap.set(name,'DESC');
        x.setAttribute("class", "fas fa-sort-down");

    }
    else if (sortOrder == 'DESC') {
        columnSortMap.set(name,null);
        x.setAttribute("class", "fas fa-sort");
    }
    else {
        columnSortMap.set (name,'ASC');
        x.setAttribute("class", "fas fa-sort-up");
    }

    requestRawData();
}


function requestRawData() {
// send request to get RawData
    //raw data
    //populateRawData("0");
    var params = constructParameter();
    var x = new XMLHttpRequest();
    x.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            let json = this.responseText;
            populateRawData(json);
        }
    };
    x.open('GET', RAWDATA_URL + params);
    x.send();

}


function populateRawData(json) {
// fill in the table
// need to know how many rows there  are - maybe returned as a field in response
    var tableData = "";
    //$.getJSON("jsonData.txt", function(json){
//        var response = JSON.parse(json)["deals"];
    var parsedJson = JSON.parse(json);
    var response = parsedJson["deals"];

    for (i = 0; i < response.length; i++) {
        var rowHtml = "<tr>";
        for (j = 0; j < RAWDATA_COLS.length; j++) {
            rowHtml += "<td>" + response[i][RAWDATA_COLS[j]] + "</td>";
        }
        rowHtml += "</tr>";
        tableData += rowHtml;
    }
    document.getElementById("tableBody").innerHTML = tableData;
    // update the number of results and number of pages
    NUM_ROWS = parsedJson["totalRows"];
    NUM_PAGES = Math.round(NUM_ROWS / ENTRY_PER_PAGE);
    showNUM_PAGES();


    //});

}

function constructParameter() {
    // maybe not send info for each column ?
    var keyWords = [];
    var filterCols = [];
    var sortOrders = [];
    var sortCols = [];
    for (i = 0; i < RAWDATA_COLS.length; i++) {
        col = RAWDATA_COLS[i];
        if (columnKeywordMap.get(col) != null) {
            keyWords.push(columnKeywordMap.get(col));
            filterCols.push(col);
        }
        if (columnSortMap.get(col) != null) {
            sortOrders.push(columnSortMap.get(col));
            sortCols.push(col);
        }

    }
    var parameter = "?pageNumber=" + PAGE_NO + "&entryNumber=" + ENTRY_PER_PAGE;
    if (sortCols.length == sortOrders.length) {
        for (i = 0; i < sortOrders.length; i++) {
            parameter += "&sortOrders=" + sortOrders[i] + "&sortColumns=" + sortCols[i];
        }
    }
    if (filterCols.length == keyWords.length) {
        for (i = 0; i < keyWords.length; i++) {
            parameter += "&filterKeyWords=" + keyWords[i] + "&filterColumns=" + filterCols[i];
        }
    }
    return parameter;
}


function filterForm() {
    key = document.getElementById("keyword").value;
    col = document.getElementById("col-dropdown").value;
    // validating values
    if (key == "" && col == "") {
        $("#keyword").addClass("invalid");
        $("#col-dropdown").addClass("invalid");
    }
    else if (key == "") {
        $("#keyword").addClass("invalid");
        $("#col-dropdown").removeClass("invalid");
    }
    else {
        if (col == "") {
            $("#keyword").removeClass("invalid");
            $("#col-dropdown").addClass("invalid");
        }
        else {
            $("#keyword").removeClass("invalid");
            $("#col-dropdown").removeClass("invalid");

            //send request
            columnKeywordMap.set(col,key);
            requestRawData();
        }

    }

}

function clearForm(){
    let col = document.getElementById('keyword').value;
    document.getElementById('keyword').value = null;
    document.getElementById('col-dropdown').value = null;
    //columnKeywordMap.set(col,null);
    columnKeywordMap.clear();
    requestRawData();
}

function pageNumberClick() {
    var page = document.getElementById("page").value;
    PAGE_NO = page;
    requestRawData();
}

function entryNumberClick() {
    var entry = document.getElementById("entry").value;
    ENTRY_PER_PAGE = entry;
    requestRawData();
}

// need a button to clear out the filter

function showNUM_PAGES() {
    document.getElementById("no-pages").innerHTML = NUM_PAGES.toString();

}

function checkSession(){
    if (IN_SESSION == true){
        clickLogin();
    }
}

function credProbe(){
    var params = constructParameter();
//    var x = new XMLHttpRequest();
//    x.onreadystatechange = function () {
//        if (this.readyState == 4 && this.status == 200) {
//            console.log("works");
//            clickLoginHelper();
//        }
//    };
//    x.open('GET', RAWDATA_URL + params);
//    x.send();
    $.get(RAWDATA_URL + params, function (data) {
        clickLoginHelper();
        disableEnter();
    });
}

function clickLoginHelper(){
       clearBox(LOGIN);
       switchVisibility(LOGIN);
       $("#" + ACTUALSITE).load(ACTUALSITE_HTML);
       switchVisibility(ACTUALSITE);
        // send 3 http requests to the back end for the three tab
       requestRawData();
       updateUser();

}

function goTOraw() {
    clearBox(ACTUALSITE);
    $("#" + ACTUALSITE).load(ACTUALSITE_HTML);
    requestRawData();
    updateUser();
}

function goTOextra(){
    clearBox(ACTUALSITE);
    $("#" + ACTUALSITE).load("extra_analysis.html", function(){
            updateUser();
    });
}

function goTOcorr() {
    clearBox(ACTUALSITE);
    $("#" + ACTUALSITE).load("correlation.html", function(){
        //doCorr();
        updateUser();
        create_scatter();
        donutChart();
    });
//    setTimeout(() => {
//    }, 100);

}

function create_scatter(){
/*
    var x = new XMLHttpRequest();
    var response;
    x.open('GET', "rws/api/deals-simple");
    x.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            let json = this.responseText;
            doCorr(json);
            }
        };
        x.send();


*/


    $.ajax({
                       type: "GET",
                       url: "rws/api/deals-simple",
                       success: function (data) {
                           doCorr(data);
                       }
            });


}

function mapNameToNo(name) {
    if (name=="Astronomica") {
        return 1.05;
    }
    if (name=="Borealis") {
        return 2;
    }
    if (name=="Celestial") {
            return 3;
        }
    if (name=="Deuteronic") {
            return 4;
        }
    if (name=="Eclipse") {
            return 5;
    }
    if (name=="Floral") {
                return 6;
        }
    if (name=="Galactia") {
                return 7;
        }
    if (name=="Heliosphere") {
                    return 8;
            }
    if (name=="Interstella") {
                    return 9;
            }
    if (name=="Jupiter") {
                        return 10;
                }
    if (name=="Koronis") {
                        return 11;
                }
    if (name=="Lunatic") {
                        return 11.95;
                }
    return 0;
}


function color(name) {
    if(name=="Jupiter") {
    return "red";
    }
    else{
    return "#000066";
    }
}

function doCorr(json){
        //var response = JSON.parse(json);
        var response = json;
        console.log(json);
//        response = parsedJson["deals"];
        // Create data
        function buildData() {
            var data = [];

            for (i = 0; i < response.length; i++) {
                data.push({
                    x: mapNameToNo(response[i]["instrumentName"]),
                    y: response[i]["amount"],
                    name: response[i]["instrumentName"]
                });
            }
            return data;
        }

        var data = buildData();

        var margin = { top: 20, right: 50, bottom: 50, left: 50 };
        width = 900 - margin.left - margin.right,
        height = 480 - margin.top - margin.bottom;

        var tooltip = d3.select("body").append("div")
            .attr("class", "tooltip")
            .style("opacity", 0);

        var x = d3.scaleLinear()
              .range([0, width])
              .nice();

        var y = d3.scaleLinear()
            .range([height, 0]);

        var xAxis = d3.axisBottom(x)
            .ticks(12.5)
            .tickFormat(function (d, i) {
                      return INSTRUMENTS[i];}),
            yAxis = d3.axisLeft(y).ticks(10*width/height );

        var brush = d3.brush().extent([[0, 0], [width+1, height]]).on("end", brushended),
            idleTimeout,
            idleDelay = 350;

        var svg = d3.select("#corrBody").append("svg")
                    .attr("width", width + margin.left + margin.right)
                    .attr("height", height + margin.top + margin.bottom)
                    .append("g")
                    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        var clip = svg.append("defs").append("svg:clipPath")
            .attr("id", "clip")
            .append("svg:rect")
            .attr("width", width )
            .attr("height", height )
            .attr("x", 0)
            .attr("y", 0);

        var xExtent = d3.extent(data, function (d) { return d.x; });
        var yExtent = d3.extent(data, function (d) { return d.y; });
        x.domain(d3.extent(data, function (d) { return d.x; })).nice();
        y.domain(d3.extent(data, function (d) { return d.y; })).nice();

        var scatter = svg.append("g")
             .attr("id", "scatterplot")
             .attr("clip-path", "url(#clip)");

        scatter.selectAll(".dot")
            .data(data)
          .enter().append("circle")
            .attr("class", "dot")
            .attr("r", 4)
            .attr("cx", function (d) { return x(d.x); })
            .attr("cy", function (d) { return y(d.y); })
            .attr("opacity", 0.5)
            .style("fill", "#4292c6");

        // x axis
        svg.append("g")
           .attr("class", "x axis")
           .attr('id', "axis--x")
           .attr("transform", "translate(0," + height + ")")
           .call(xAxis);

        svg.append("text")
         .style("text-anchor", "end")
            .attr("x", width)
            .attr("y", height - 8)
         .text("");

        // y axis
        svg.append("g")
            .attr("class", "y axis")
            .attr('id', "axis--y")
            .call(yAxis);

        svg.append("text")
            .attr("transform", "rotate(-90)")
            .attr("y", 6)
            .attr("dy", "1em")
            .style("text-anchor", "end")
            .text("Amount");

        scatter.append("g")
            .attr("class", "brush")
            .call(brush);



        function brushended() {

            var s = d3.event.selection;
            if (!s) {
                if (!idleTimeout) return idleTimeout = setTimeout(idled, idleDelay);
                x.domain(d3.extent(data, function (d) { return d.x; })).nice();
                y.domain(d3.extent(data, function (d) { return d.y; })).nice();
            } else {

                x.domain([s[0][0], s[1][0]].map(x.invert, x));
                y.domain([s[1][1], s[0][1]].map(y.invert, y));
                scatter.select(".brush").call(brush.move, null);
            }
            zoom();
        }

        function idled() {
            idleTimeout = null;
        }

        function zoom() {

            var t = scatter.transition().duration(750);
            svg.select("#axis--x").transition(t).call(xAxis);
            svg.select("#axis--y").transition(t).call(yAxis);
            scatter.selectAll("circle").transition(t)
            .attr("cx", function (d) { return x(d.x); })
            .attr("cy", function (d) { return y(d.y); });
        }
}

function donutChart(){
            var chart = new CanvasJS.Chart("donutChart", {
                    animationEnabled: true,
                    title:{
                            text: "",
                            horizontalAlign: "left"
                    },
                    data: [{
                            type: "doughnut",
                            startAngle: 60,
                            //innerRadius: 60,
                            indexLabelFontSize: 17,
                            indexLabel: "{label} - #percent%",
                            toolTipContent: "<b>{label}:</b> {y} (#percent%)",
                            dataPoints: [
                                    { y: 167, label: "Astronomica" },
                                    { y: 181, label: "Borealis" },
                                    { y: 147, label: "Celestial" },
                                    { y: 176, label: "Deuteronic"},
                                    { y: 168, label: "Eclipse"},
                                    { y: 170, label: "Floral"},
                                    { y: 174, label: "Galactia" },
                                    { y: 139, label: "Heliosphere" },
                                    { y: 155, label: "Interstella" },
                                    { y: 163, label: "Jupiter"},
                                    { y: 187, label: "Koronis"},
                                    { y: 173, label: "Lunatic"}
                            ]
                    }]
            });
            chart.render();
}


function enableEnter(){
    $(document).keydown(function(e){
      var key = e.charCode || e.keyCode;
      if (key == 13) {
        clickLogin();
      }
    });
}

function disableEnter(){
    $(document).on('keypress',function(e){
          var key = e.charCode || e.keyCode;
          if (key == 13) {
            return false;
          }
        });
}

function updateUser(){
    $.ajax({
                   type: "GET",
                   url: USER_URL,
                   success: function (data) {
                       $("#username").html(" "+data);
                   }
        });
}

function avgdata(json){
    let colnames = ["id","buy","sell"];
    $('#current_page').empty();
    $('#current_page').append(
        '<table class="table table-striped" > <thead> <tr> <th scope="col" >Instrument<i id = "Instrument Name" class="fas fa-sort" ></i></th><th scope="col"> Buy <i id = "Price" class="fas fa-sort"></i></th><th scope="col"> Sell <i id = "Difference" class="fas fa-sort"></i></th></thead><tbody id = "tableBody"></tbody></table>'
    );
    var tableData = "";
    for (i = 0; i < json.length; i++) {
        var rowHtml = "<tr>";
        for (j = 0; j < 3; j++) {
            rowHtml += "<td>" + json[i][colnames[j]] + "</td>";
        }
        rowHtml += "</tr>";
        tableData +=rowHtml;
    }
    $("#tableBody").html(tableData);
}

function avgdata_q1() {

          $.ajax({
                       type: "GET",
                       url: "rws/api/req-1",
                       success: function (data) {
                            avgdata(data);

                       }
            });


}

function ending_positions(){
    $('#current_page').empty();
    $('#current_page').append(
        '<table class="table table-striped" > <thead> <tr> <th scope="col" >Instrument ID <i id = "Instrument Name" class="fas fa-sort" ></i></th><th scope="col"> Price <i id = "Price" class="fas fa-sort"></i></th><th scope="col"> Difference <i id = "Difference" class="fas fa-sort"></i></th></thead><tbody id = "tableBody"></tbody></table>'
    );
    var tableData = "";
        for (i = 0; i < 10; i++) {
            var rowHtml = "<tr>";
            for (j = 0; j < 3; j++) {
                rowHtml += "<td>" + (i * j) + "</td>";
            }
            rowHtml += "</tr>";
            tableData +=rowHtml;
        }
     document.getElementById("tableBody").innerHTML = tableData;
}

function realised_pl(){
        let colnames = ["id","result"];
     $.ajax({
                           type: "GET",
                           url: "rws/api/req-3",
                           success: function (data) {
                                $('#current_page').empty();
                                    $('#current_page').append(
                                        '<table class="table table-striped" > <thead> <tr> <th scope="col" > User <i id = "Instrument Name" class="fas fa-sort" ></i></th><th scope="col"> Profit/Loss <i id = "Price" class="fas fa-sort"></i></th></thead><tbody id = "tableBody"></tbody></table>'
                                    );
                                    var tableData = "";
                                        for (i = 0; i < data.length; i++) {
                                            var rowHtml = "<tr>";
                                            for (j = 0; j < 2; j++) {
                                                rowHtml += "<td>" + data[i][colnames[j]] + "</td>";
                                            }
                                            rowHtml += "</tr>";
                                            tableData +=rowHtml;
                                        }
                                        $("#tableBody").html(tableData);


                           }
                });


}

function actual_pl(){
    $('#current_page').empty();
    $('#current_page').append(
        '<table class="table table-striped" > <thead> <tr> <th scope="col" >Instrument ID <i id = "Instrument Name" class="fas fa-sort" ></i></th><th scope="col"> Price <i id = "Price" class="fas fa-sort"></i></th><th scope="col"> Difference <i id = "Difference" class="fas fa-sort"></i></th></thead><tbody id = "tableBody"></tbody></table>'
    );
    var tableData = "";
        for (i = 0; i < 10; i++) {
            var rowHtml = "<tr>";
            for (j = 0; j < 3; j++) {
                rowHtml += "<td>" + (j) + "</td>";
            }
            rowHtml += "</tr>";
            tableData +=rowHtml;
        }
        document.getElementById("tableBody").innerHTML = tableData;
}

function stacked_bar(){
    var data=
        {
        "04:07": 1940,
        "01:55": 4077,
        "04:08": 1117,
        "01:54": 2524,
        "04:09": 758,
        "05:59": 488,
        "05:58": 3362,
        "04:47": 107,
        "06:29": 1185,
        "04:42": 53,
        "01:53": 712,
        "04:40": 56,
        "09:53": 2383,
        "09:52": 7030,
        "14:50": 751,
        "06:59": 645,
        "05:49": 1986,
        "04:38": 1043,
        "02:51": 4248,
        "02:52": 6218,
        "06:58": 9825,
        "02:53": 1952,
        "09:48": 667,
        "05:45": 566,
        "09:45": 1046,
        "05:42": 1065,
        "05:41": 4713,
        "07:20": 1597,
        "09:40": 2459,
        "14:29": 6232,
        "14:27": 9229,
        "03:16": 22,
        "02:06": 1901,
        "03:18": 5779,
        "02:07": 519,
        "49:53": 6319,
        "07:19": 5486,
        "02:02": 4233,
        "07:16": 6239,
        "07:17": 12752,
        "03:15": 3909,
        "04:20": 4016,
        "07:14": 2680,
        "07:15": 6114,
        "04:21": 593,
        "06:00": 1873,
        "04:22": 6239,
        "09:37": 7805,
        "06:02": 9328,
        "09:36": 9165,
        "04:23": 1089,
        "09:35": 2997,
        "07:52": 6074,
        "14:33": 2560,
        "14:32": 3837,
        "14:31": 3380,
        "14:30": 1051,
        "03:05": 4975,
        "04:17": 3869,
        "03:06": 677,
        "04:19": 644,
        "03:08": 1041,
        "07:07": 2564,
        "04:13": 48,
        "04:14": 1994,
        "03:02": 538,
        "07:08": 481,
        "03:03": 5513,
        "07:05": 2,
        "03:04": 2701,
        "04:16": 7450,
        "07:06": 4251,
        "04:10": 5688,
        "07:04": 5263,
        "04:11": 5497,
        "04:12": 8406,
        "06:30": 2103,
        "07:00": 512,
        "58:20": 4794,
        "58:21": 5948,
        "14:49": 3340,
        "58:24": 2462,
        "14:43": 387
    };
    Object.keys(data).forEach(function(key) {
      console.log('Key : ' + key + ', Value : ' + data[key]);
    });
}